const initialState = {
    comments: [],
    fetching: false,
    successfully: false,
    error: null,
}

const commentReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'COMMENTS_FETCH_START': 
            return {
                ...state,
                fetching: true
            };
        case 'COMMENTS_FETCH_SUCCESS':
            return {
                ...state,
                comments: action.comments,
                fetching: false,
                successfully: true
            };
        case 'COMMENTS_FETCH_FAILURE': 
            return {
                ...state,
                fetching: false,
                successfully: false,
                error: action.error
            }
        default: 
            return state;
    }
}

export default commentReducer;