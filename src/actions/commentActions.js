const commentsFetch = (inputString) => {
    return (dispatch) => {
        dispatch(commentsFetchStart());
        
        fetch('https://jsonplaceholder.typicode.com/comments', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors'
        })
        .then(data => data.json())
        .then(posts => {
            let filteredPosts = posts.filter(post => post.body.indexOf(inputString) !== -1);

            dispatch(commentsFetchSuccess(filteredPosts))
        })
        .catch(error => dispatch(commentsFetchFailure(error)))
    }
}

const commentsFetchStart = () => {
    return {type: 'COMMENTS_FETCH_START'}
}
const commentsFetchSuccess = (comments) => {
    return {type: 'COMMENTS_FETCH_SUCCESS', comments: comments} 
}
const commentsFetchFailure = (error) => {
    return {type: 'COMMENTS_FETCH_FAILURE', error: error}
}

export default commentsFetch;