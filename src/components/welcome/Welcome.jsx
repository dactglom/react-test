import React from 'react';
import Typography from '@material-ui/core/Typography';

const textStyle = {
    marginTop: '20%'
}   

const Welcome = () => {
    return (
        <div className='welcome'>
            <Typography 
                className='welcome__heading'
                align='center'
                component='h2'
                variant='h4'
                color='primary'
                style={textStyle}
            >
                WELCOME!
            </Typography>
            <Typography 
                className='welcome__text'
                align='center'
                component='h1'
                variant='h5'
                color='primary'
                style={textStyle}
            >
                Start by entering a string in the input above!
            </Typography>
        </div>
    )
}

export default Welcome;