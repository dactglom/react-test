import React from 'react';
import Typography from '@material-ui/core/Typography';

const emptyResultStyle = {
    marginTop: '40%'
}

const EmptyResult = () => {
    return (
        <div className="empty-result">
            <Typography
                variant='h4'
                color='textSecondary'
                component='h2'
                style={emptyResultStyle}
                align='center'
            >
                Search has given no results
            </Typography>
        </div>
    )
}

export default EmptyResult;