import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const cardStyle = {
    maxWidth: '270px',
    margin: '10px 15px'
}

const horizontalMargin = {
    margin: '10px 0'
}

const CommentCard = ({ name, email, text, highlight }) => {
    const showHighlighted = (text, highlight) => {
        let parts = text.split(new RegExp(`(${highlight})`, 'gi'));
        return <Typography
            component={'p'}
            variant={'body1'}
            color={'textPrimary'}
            style={horizontalMargin}
        >
            {
                parts.map((part, i) =>
                    <span key={i} style={part === highlight ? { fontWeight: '600', color: 'orange' } : {}}>
                        {part}
                    </span>
                )
            }
        </Typography>
    }

    return (
        <Card
            style={cardStyle}
        >
            <CardContent>
                <Typography
                    variant={'h6'}
                    component={'h1'}
                    color={'textPrimary'}
                    style={horizontalMargin}
                >
                    {name.toUpperCase()}
                </Typography>
                <Typography
                    variant={'subtitle2'}
                    component={'h1'}
                    color={'textSecondary'}
                    style={horizontalMargin}
                >
                    {email}
                </Typography>
                {
                    showHighlighted(text, highlight)
                }
            </CardContent>
        </Card>
    )
}

export default CommentCard;