import React from 'react';
import './app.css';
import { connect } from 'react-redux';
import commentsFetch from '../../actions/commentActions';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import CommentCard from '../commentCard/CommentCard';
import Welcome from '../welcome/Welcome';
import EmptyResult from '../emptyResult/EmptyResult';

const buttonStyle = {
	backgroundColor: '#303f9f',
	color: '#fff',
	width: '27%'
};

const inputStyle = {
	width: '70%'
};

const textStyle = {
	marginRight: '10px'
}

const chipStyle = {
	margin: '0 5px'
}

class App extends React.Component {
	constructor() {
		super();
		this.state = {
			inputString: '',
			requested: false,
			emptyResult: null,
			previousRequests: [],
			requestedString: ''
		}
		this.textInput = React.createRef();
	}

	onChange = (e) => {
		this.setState({
			inputString: e.target.value
		});
	}

	onClick = () => {
		if (this.state.inputString) {
			this.setState({
				requested: true,
				requestedString: this.state.inputString
			}, () => {
				this.props.getComments(this.state.inputString)
				this.state.previousRequests.push(this.state.inputString);
			});
		}
	}

	chipClick = (chipsLabel) => {
		this.textInput.current.value = chipsLabel;
		this.setState({
			requestedString: chipsLabel,
			inputString: chipsLabel
		});
		this.props.getComments(chipsLabel);
	}

	render() {
		const { isFetching, comments } = this.props;
		const { requested, previousRequests, requestedString } = this.state;

		return (
			<div className="app">
				<div className="app__inner">
					<div className="previous-requests">
						<Typography
							variant='subtitle2'
							color='primary'
							style={textStyle}
						>
							Previous requests:
      					</Typography>
						{
							previousRequests && previousRequests.map((request, id) => {
								return <Chip key={id} style={chipStyle} label={request} onClick={() => this.chipClick(request)} />
							})
						}
					</div>
					<div className="wrapper wrapper--input-button">
						<TextField
							placeholder={'Enter a text...'}
							onChange={e => this.onChange(e)}
							style={inputStyle}
							inputRef={this.textInput}
						/>
						<Button
							style={buttonStyle}
							onClick={() => this.onClick()}
						>
							SUBMIT
      					</Button>
					</div>
					{
						requested
							? null
							: <Welcome />
					}
					{
						isFetching
							? <div className={'loading-bar'}>
								<CircularProgress className={'loading-bar__item'} />
							</div>
							: (!comments.length && requested)
								? <EmptyResult />
								: <div className={'comments-wrapper'}>
									{comments.map((comment, id) => {
										return <CommentCard
											key={id}
											name={comment.name}
											email={comment.email}
											text={comment.body}
											highlight={requestedString}
										/>
									})}
								</div>
					}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		isFetching: state.commentReducer.fetching,
		comments: state.commentReducer.comments
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getComments: (inputString) => {
			dispatch(commentsFetch(inputString))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);